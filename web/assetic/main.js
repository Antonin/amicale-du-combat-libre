$( document ).ready(function() {

    $('#menu').affix({
        offset: {
            top: function() {
                return Math.max(
                                300-$("#menu").height(),
                                Math.min(
                                    550,
                                    ($(window).height()/2)-$("#menu").height()
                                )
                );
            }
        }
    });

});