<?php

namespace ACL\Bundle\ACLBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Application\Sonata\PageBundle\Entity\Page;

class MenuController extends Controller
{
    public function listAction(Request $request)
    {

        $em = $this->getDoctrine()->getRepository('ApplicationSonataPageBundle:Page');
        $query = $em->createQueryBuilder('p')
            ->where('p.inMenu = :inMenu')
            ->setParameter('inMenu', true)
            ->orderBy('p.position', 'ASC')
            ->getQuery();
        $results = $query->getResult();

        $pagesList = array();
        foreach ($results as $page) {
            $this->addToPageList($pagesList, $page->getTitle(), $page->getUrl());
        }

        $router = $this->get('router');

        $this->addToPageList($pagesList, "Blog", $router->generate('sonata_news_home'));
        $this->addToPageList($pagesList, "Contact", $router->generate('mremi_contact_form'));

        $securityContext = $this->container->get('security.context');

        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $user = $securityContext->getToken()->getUser();
            $this->addToPageList($pagesList, $user->getUsername(), $router->generate('sonata_user_profile_show'), 'glyphicon glyphicon-user');
            $this->addToPageList($pagesList, "", $router->generate('sonata_user_security_logout'), 'glyphicon glyphicon-log-out');
        } else {
            $this->addToPageList($pagesList, "Connexion", $router->generate('sonata_user_security_login'), 'glyphicon glyphicon-log-in');
        }


        return $this->render('ACLBundle:Default:menu.html.twig', array('pages' => $pagesList));
    }

    private function addToPageList(&$pagesList, $name, $url, $icon='')
    {
        $pagesList[] = array(
            'name' => $name,
            'url' => $url,
            'icon' => $icon
        );
    }
}
