<?php

namespace ACL\Bundle\ACLBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HelloController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ACLBundle:Default:hello.html.twig', array('name' => $name));
    }
}
