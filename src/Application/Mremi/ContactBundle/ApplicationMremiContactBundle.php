<?php
/**
 * Created by PhpStorm.
 * User: antonin
 * Date: 11/10/15
 * Time: 12:02 PM
 */

namespace Application\Mremi\ContactBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ApplicationMremiContactBundle extends Bundle
{

    public function getParent()
    {
        return 'MremiContactBundle';
    }
}