<?php
/**
 * Created by PhpStorm.
 * User: antonin
 * Date: 11/10/15
 * Time: 1:51 PM
 */

namespace Application\Mremi\ContactBundle\Type;

use Mremi\ContactBundle\Model\Contact;
use Mremi\ContactBundle\Provider\SubjectProviderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Mremi\ContactBundle\Form\Type\ContactType as BaseContactType;


class ContactType extends AbstractType
{
    /**
     * @var SubjectProviderInterface
     */
    private $subjectProvider;

    /**
     * @var string
     */
    private $class;

    /**
     * @var string
     */
    private $captchaType;

    /**
     * Constructor.
     *
     * @param SubjectProviderInterface $subjectProvider A subject provider instance
     * @param string                   $class           The Contact class namespace
     * @param string                   $captchaType     The captcha type
     */
    public function __construct(SubjectProviderInterface $subjectProvider, $class, $captchaType)
    {
        $this->subjectProvider = $subjectProvider;
        $this->class           = $class;
        $this->captchaType     = $captchaType;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('title')
            ->add('firstName', 'text',  array('label' => 'mremi_contact.form.first_name'))
            ->add('lastName',  'text',  array('label' => 'mremi_contact.form.last_name'))
            ->add('email',     'email', array('label' => 'mremi_contact.form.email'));

        if ($subjects = $this->subjectProvider->getSubjects()) {
            $builder
                ->add('subject', 'choice', array(
                    'choices' => $subjects,
                    'label'   => 'mremi_contact.form.subject',
                ));
        } else {
            $builder->add('subject', 'text', array('label' => 'mremi_contact.form.subject'));
        }

        $builder->add('message', 'textarea', array('label' => 'mremi_contact.form.message'));

        if ($this->captchaType) {
            $builder->add('captcha', $this->captchaType, array(
                'label' => 'mremi_contact.form.captcha',
            ));
        }

        $builder->add('save', 'submit', array('label' => 'mremi_contact.form_submit'));
    }

    /**
     * {@inheritdoc}
     *
     * @todo: Remove it when bumping requirements to Symfony 2.7+
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => $this->class,
            'intention'          => 'contact',
            'translation_domain' => 'MremiContactBundle',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'application_contact';
    }

} 